/// A tic-tac-toe game by M�rt M�emees
/// The main.cpp acts as the view for the game.

#include <iostream>
#include <string>

#include "TicTacToe.h"

TicTacToe GameInstance;

int main()
{
	bool playing = false;

	do
	{
		GameInstance = TicTacToe();
		Players winner = Players::None;

		while (winner == Players::None && !GameInstance.IsFieldFull())
		{
			int row = 0, col = 0;

			GameInstance.DrawField();
			// Get the input from user.
			std::cout << "Please enter the row you wish to mark (1-3): ";
			std::cin >> row;
			std::cout << "Please enter the column you wish to mark (1-3): ";
			std::cin >> col;
			// Substract 1 from both values, to make it compatible with array indexing.
			row--; 
			col--;
			
			// TODO FIX: When user inputs characters, the game gets stuck in an endless loop.

			// Check for the inputs to be valid.
			if (row < 0 || row > 2 || col < 0 || col > 2)
			{
				std::cout << "The input was invalid. Please make sure to enter values of 1 to 3." << std::endl;
				continue;
			}

			// If the chosen field is already marked, go back to the beginning of the while loop to ask again.
			if (!GameInstance.MarkField(col + row * 3, Players::Player))
			{
				continue;
			}
			winner = GameInstance.CheckWin(Players::Player) ? Players::Player : Players::None;

			// If the player hasn't won and there is room on the table, process an AI turn.
			if (winner == Players::None && !GameInstance.IsFieldFull())
			{
				GameInstance.AITurn();
				winner = GameInstance.CheckWin(Players::AI) ? Players::AI : Players::None;
			}
		}

		GameInstance.DrawField();
		if (winner != Players::None)
		{
			std::cout << "Game has been won by: " << (winner == Players::Player ? "Player" : "AI") << std::endl;
		}
		else
		{
			std::cout << "The game ended in a draw." << std::endl;	
		}

		std::string answer = "";
		std::cout << "Do you want to play again? (Y/N) ";
		// TODO Figure out what the hell.
		//std::getline(std::cin, answer); // Does not work for some reason. 
		std::cin >> answer;
		playing = answer[0] == 'y' || answer[0] == 'Y' ? true : false;
		std::cout << std::endl;
	} while (playing);
	return 0;
}