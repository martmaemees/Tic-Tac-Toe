/// Author: M�rt M�emees

#include "TicTacToe.h"
// Constructor
TicTacToe::TicTacToe()
{
	field = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
}

// Draws the field to the console, using 'X' for Player and '0' for AI.
void TicTacToe::DrawField()
{
	std::cout << "-------" << std::endl;
	std::cout << '|' << GetChar(0) << '|' << GetChar(1) << '|' << GetChar(2) << '|' << std::endl;
	std::cout << '|' << GetChar(3) << '|' << GetChar(4) << '|' << GetChar(5) << '|' << std::endl;
	std::cout << '|' << GetChar(6) << '|' << GetChar(7) << '|' << GetChar(8) << '|' << std::endl;
	std::cout << "-------" << std::endl;
}

// Attempts to mark a field for a player. If the given field is already marked, returns false.
bool TicTacToe::MarkField(int fieldIndex, Players player)
{
	if (field[fieldIndex] != 0)
	{
#if _DEBUG_
		std::cout << "Entered fieldIndex for MarkField has already been marked. Index: " << fieldIndex << std::endl;
#endif
		return false;
	}
	if (player == Players::None)
	{
#if _DEBUG_
		std::cout << "player cannot be None for MarkField." << std::endl;
#endif
		return false;
	}
	field[fieldIndex] = (player == Players::Player ? 1 : -1);
	return true;
}

// Checks if the given player has won.
bool TicTacToe::CheckWin(Players player)
{
	int sum = player == Players::Player ? 3 : -3;

	for (int i = 0; i < 3; i++)
	{
		if (field[i * 3] + field[1 + i * 3] + field[2 + i * 3] == sum)
			return true;
		if (field[i] + field[i + 3] + field[i + 6] == sum)
			return true;
	}
	if (field[0] + field[4] + field[8] == sum || field[2] + field[4] + field[6] == sum)
		return true;
	return false;
}

// Gets a char for marking the position at fieldIndex.
char TicTacToe::GetChar(int fieldIndex)
{
	if (field[fieldIndex] == 1)
		return 'X';
	if (field[fieldIndex] == -1)
		return '0';
	return ' ';
}

// Checks if the field is full.
bool TicTacToe::IsFieldFull()
{
	for (int i = 0; i < 9; i++)
	{
		if (field[i] == 0)
			return false;
	}
	return true;
}

// TODO Make an actual AI.
// Processes one turn for the AI.
void TicTacToe::AITurn()
{
	std::vector<int> turnOrder = {4, 1, 3, 5, 7, 0, 2, 6, 8};
	bool played = false;
	int index = 0;
	while (!played)
	{
		played = MarkField(turnOrder[index], Players::AI);
		index++;
	}
}