/// Controller and model for the Tic-Tac-Toe game by M�rt M�emees

#pragma once

#include <vector>
#include <iostream>

enum class Players
{
	None, Player, AI
};

class TicTacToe
{
public:
	TicTacToe();
	void DrawField();
	bool MarkField(int, Players);
	bool CheckWin(Players);
	void AITurn();
	bool IsFieldFull();

private:
	std::vector<int> field;

	char GetChar(int);
};

